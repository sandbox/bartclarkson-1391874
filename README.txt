Description:
------------

The Demandbase for Webform module exists to simplify usage of the "Demandbase 
for Web Forms" product. This module allows content managers to populate the 
components of a Webform (3.x) with values from Demandbase.

http://www.demandbase.com/resources/product-brochures/demandbase-for-web-forms/

An existing subscription with demandbase.com is expected, the key for which is 
entered in the configuration page.

Actively under development by significode! LLC, feedback regarding this module 
is welcomed. If requests for additional functionality are non-trivial, 
significode! LLC will be pleased to negotiate a development rate.


Requirements:
-------------
Webform module (7.x) must installed and enabled.


Setup:
______
 
At admin/settings/demandbase, enter the Demandbase key associated with your 
account. If you would like to limit the number Demandbase values available 
to content managers, you can do so there. OTOH, should Demandbase support a 
value not seen, it can be added.


Basic Usage Example:
____________________

1. Enable this module.
2. Configure your key (admin/settings/demandbase).
3. Add a Webform.
4. On the main Edit page of the webform, check the Demandbase IP Lookup 
   checkbox. Save.
5. Add a textarea component.
6. On the textarea component edit page, select Catch-all in the IP Lookup 
   Field Name select menu. Save.
7. View the form.


Additional Information:
_______________________

The following components are supported for mapping of Demandbase fields:

textfield
textarea
email
hidden

Two types of integration with Demandbase are supported, and are not mutually 
exclusive. "IP Lookup," which uses the user's IP to query against Demandbase, 
and the "Company Autocomplete" product, which uses the textfield you've 
designated as "Company" within the Demandbase select menu of the given component
to return (sometimes) better results. Additionally, the Company Autocomplete 
integrates with Demandbase's Email API, utilizing the component you've 
designated as "Email," and can return the name and job title for certain people.

The module is constructed such that all fields provided by Demandbase are 
supported by default in the admin configuration page. This has been left 
open for both the IP Lookup and the Company Autocomplete, however, in the 
event that either more fields become available from Demandbase, or you wish 
to limit what is made available to content managers.

Applying the "Catch-all" Demandbase mapping to a webform component is a fairly 
painless way to simply pass a raw transcript of user characteristics to your 
sales team, and to get a sense of the cms workflow of this module if you're 
just checking it out.

Presently (01/01/2012), the Default fieldset returned by Demandbase for basic 
IP and Company lookup is as follows:

annual_sales
city
company_name
country
country_name
demandbase_sid
employee_count
employee_range
fortune_1000
forbes_2000
industry
ip
isp
latitude
longitude
location_contacts
marketing_alias
phone
primary_sic
registry_area_code
registry_city
registry_company_name
registry_country
registry_country_code
registry_latitude
registry_longitude
registry_state
registry_zip_code
revenue_range
state
street_address
stock_ticker
sub_industry
total_contacts
web_site
zip

Email API integration can additionally return the following:

first_name
last_name
title


Friendly Reminders:
___________________

Fair use of this data and associated collection techniques are between your 
company, your users, Demandbase, and relevant governing authorities.

Questions about Demandbase should be directed to Demandbase.